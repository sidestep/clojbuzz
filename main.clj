
(defn divisible? [number divisor]
  (zero? (mod number divisor)))

(defn fizz-buzz[n]
  (cond 
    (divisible? n 15) "FizzBuzz"
    (divisible? n 3)  "Fizz"
    (divisible? n 5)  "Buzz"
    :else n))

(defn declarative-fizz-buzz[n]
  (or
    (when (and 
            (divisible? n 3) 
            (not (divisible? n 5))) "Fizz")
    (when (and 
            (divisible? n 5) 
            (not (divisible? n 3))) "Buzz")
    (when (and 
            (divisible? n 3)
            (divisible? n 5))       "FizzBuzz")
    n))

(defn clever-fizz-buzz[n]
  (cond-> nil 
    (divisible? n 3) (str "Fizz")
    (divisible? n 5) (str "Buzz")
    :else (or (str n))))

(comment ;--- tests ---

(map fizz-buzz (range 1 16))
(map clever-fizz-buzz (range 1 16))
(map declarative-fizz-buzz (range 1 16))

(def test-nums (range -1000000 1000000))

(assert (= (map fizz-buzz test-nums)
           (map declarative-fizz-buzz test-nums)))

(time
  (do
    (map fizz-buzz test-nums) 
    nil))

(time
  (do 
    (map declarative-fizz-buzz test-nums)
    nil))

(time
  (do 
   (map clever-fizz-buzz test-nums)
    nil))
)
